﻿using System;

namespace LINQ_Training
{
    //Define a delegate
    public delegate bool MotorcycleDelegate(int odometer);

    class Program
    {
        static void Main()
        {
            //Initializes delegate
            //var motoDelegate = new MotorcycleDelegate(GoToService); //one option to init delegate
            MotorcycleDelegate motoDelegate = GoToService;

            //Invoke delegate
            //var result = motoDelegate.Invoke(Int32.MaxValue); //explicit syntax
            var result = motoDelegate(Int32.MaxValue); //most comon approach
           
            Console.WriteLine(result);
            Console.Read();
        }

        //Define a static GoToService method that matches delegate definition
        static bool GoToService(int odometer)
        {
            return odometer > 20000;
        }
    }
}
