﻿using System;
using System.Collections.Generic;

namespace LooseCoupling
{
    //Define a delegate that accepts an INT32 and returns bool 
    public delegate bool MotorcycleDelegate(int odometer);

    class Program
    {
        static void Main()
        {
            //Init Motorcycle collection
            var motorcycles = new List<Motorcycle>
            {
                new Motorcycle {Model = "Honda CB600F", Odometer = 30000},
                new Motorcycle {Model = "Minsk X250", Odometer = 200},
                new Motorcycle {Model = "Hoda VTX 1800", Odometer = 5000},
                new Motorcycle {Model = "Suzuki Bandit 600", Odometer = 55000}
            };

            #region Anonimous Methods

            var maxOdometer = 20000;
            Func<int, bool> funcDel = delegate(int odometer)
            {
                return odometer > maxOdometer;
            };

            //var results = Where(motorcycles, funcDel);

            #endregion

            #region Lambda Expression

            //anonimous approach
            //var results = Where(motorcycles, delegate (int odometer)
            //{
            //    return odometer > maxOdometer;
            //});

            //lambda approach
            //var results = Where(motorcycles, od => od > maxOdometer); 

            #endregion

            var results = Where(motorcycles, GoToService);
            foreach (var motorcycle in results)
            {
                Console.WriteLine($"{motorcycle.Model} - {motorcycle.Odometer}");
            }

            Console.Read();
        }

        //Define a static GoToService method that matches delegate definition
        static bool GoToService(int odometer)
        {
            return odometer > 20000;
        }

        //Method that filtres a Motorcycle collection
        static List<Motorcycle> Where(IEnumerable<Motorcycle> motorcycles, MotorcycleDelegate motoDel)
        //static List<Motorcycle> Where(IEnumerable<Motorcycle> motorcycles, Func<int, bool> motoDel)   //Func<T, TResult> sample
        {
            var result = new List<Motorcycle>();
            foreach (var moto in motorcycles)
            {
                if (motoDel(moto.Odometer)) //Invoke delegate
                    result.Add(moto);
            }
            return result;
        }
    }

    #region Motocycle entity

    class Motorcycle
    {
        public string Model { get; set; }
        public DateTime Year { get; set; } 
        public MotoType Type { get; set; }
        public int Odometer { get; set; }
    }
    enum MotoType
    {
        Classic = 0,
        Cruiser = 1,
        SportBike = 2,
        OffRoad = 3
    }

#endregion
}
